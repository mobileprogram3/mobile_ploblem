abstract class AppLayout {
  static const double widthFirstBreakPoint = 450.0;
  static const double maximumContentWidthDetail = 700.0;
}
